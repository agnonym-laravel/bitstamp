# Laravel Bitstamp

Laravel client for [Bitstamp.net HTTP API v2](https://www.bitstamp.net/api/).

## Installation

You can install the package via composer:

```bash
composer require agnonym-laravel/bitstamp
```

## Usage

```php

use Bitstamp;

// Get Ticker for a trading pair (open, last, high, low prices, bid, ask orders, volume).
Bitstamp::ticker('BTC/USD');

// Get Hourly Ticker for a trading pair (open, last, high, low prices, bid, ask orders, volume).
Bitstamp::hourlyTicker('BTC/USD');

// Get Order Book for a trading pair (asks and bids lists).
Bitstamp::orderBook('BTC/USD');

// Get trading pairs information (for each trading pair: name, description, URL symbol, trading status, decimals for base and counter currency, minimum order).
Bitstamp::tradingPairsInformation();

// Get trading pair information (name, description, URL symbol, trading status, decimals for base and counter currency, minimum order).
Bitstamp::tradingPairInformation('BTC/USD');
```

## Testing

```bash
composer test
```

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
