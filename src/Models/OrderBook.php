<?php

namespace Agnonym\Bitstamp\Models;

class OrderBook extends BaseModel
{
    /**
     * List of bids open orders.
     *
     * @return array
     */
    public function bids(): array
    {
        return $this->data['bids'];
    }

    /**
     * List of asks open orders.
     *
     * @return array
     */
    public function asks(): array
    {
        return $this->data['asks'];
    }

    /**
     * Unix timestamp date and time.
     *
     * @return int
     */
    public function timestamp(): int
    {
        return (int) $this->data['timestamp'];
    }
}
