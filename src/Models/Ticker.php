<?php

namespace Agnonym\Bitstamp\Models;

class Ticker extends BaseModel
{
    /**
     * Last price.
     *
     * @return float
     */
    public function lastPrice(): float
    {
        return (float) $this->data['last'];
    }

    /**
     * Last 24 hours price high.
     *
     * @return float
     */
    public function highPrice(): float
    {
        return (float) $this->data['high'];
    }

    /**
     * Last 24 hours price low.
     *
     * @return float
     */
    public function lowPrice(): float
    {
        return (float) $this->data['low'];
    }

    /**
     * Last 24 hours volume weighted average price.
     *
     * @return float
     */
    public function vwapPrice(): float
    {
        return (float) $this->data['vwap'];
    }

    /**
     * Last 24 hours volume.
     *
     * @return float
     */
    public function volume(): float
    {
        return (float) $this->data['volume'];
    }

    /**
     * Highest buy order.
     *
     * @return float
     */
    public function bidPrice(): float
    {
        return (float) $this->data['bid'];
    }

    /**
     * Lowest sell order.
     *
     * @return float
     */
    public function askPrice(): float
    {
        return (float) $this->data['ask'];
    }

    /**
     * Unix timestamp date and time.
     *
     * @return int
     */
    public function timestamp(): int
    {
        return (int) $this->data['timestamp'];
    }

    /**
     * First price of the day.
     *
     * @return float
     */
    public function openPrice(): float
    {
        return (float) $this->data['open'];
    }
}
