<?php

namespace Agnonym\Bitstamp\Models;

class TradingPair extends BaseModel
{
    /**
     * Trading pair.
     *
     * @return string
     */
    public function name(): string
    {
        return $this->data['name'];
    }

    /**
     * URL symbol of trading pair.
     *
     * @return string
     */
    public function urlSymbol(): string
    {
        return $this->data['url_symbol'];
    }

    /**
     * Decimal precision for base currency (BTC/USD - base: BTC).
     *
     * @return int
     */
    public function baseDecimals(): int
    {
        return (int) $this->data['base_decimals'];
    }

    /**
     * Decimal precision for counter currency (BTC/USD - counter: USD).
     *
     * @return int
     */
    public function counterDecimals(): int
    {
        return (int) $this->data['counter_decimals'];
    }

    /**
     * Minimum order size.
     *
     * @return string
     */
    public function minimumOrder(): string
    {
        return $this->data['minimum_order'];
    }

    /**
     * Trading engine status (Enabled/Disabled).
     *
     * @return bool
     */
    public function trading(): bool
    {
        return ('Enabled' === $this->data['trading'] ? true : false);
    }

    /**
     * Trading pair description.
     *
     * @return string
     */
    public function description(): string
    {
        return $this->data['description'];
    }
}
