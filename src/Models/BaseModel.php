<?php

namespace Agnonym\Bitstamp\Models;

abstract class BaseModel
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Model data as array.
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
