<?php

namespace Agnonym\Bitstamp;

use Illuminate\Support\ServiceProvider;
use Agnonym\Bitstamp\Commands\BitstampCommand;

class BitstampServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/bitstamp.php' => config_path('bitstamp.php'),
            ], 'config');
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/bitstamp.php', 'bitstamp');

        $this->app->bind('bitstamp', function ($app) {
            return new Bitstamp;
        });
    }
}
