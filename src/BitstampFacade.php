<?php

namespace Agnonym\Bitstamp;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Agnonym\Bitstamp\Bitstamp
 */
class BitstampFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'bitstamp';
    }
}
