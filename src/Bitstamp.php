<?php

namespace Agnonym\Bitstamp;

use Agnonym\Bitstamp\Models\OrderBook;
use Agnonym\Bitstamp\Models\Ticker;
use Agnonym\Bitstamp\Models\TradingPair;
use Agnonym\Bitstamp\Models\TradingPairCollection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class Bitstamp
{
    const API_URL = 'https://www.bitstamp.net/api';
    const API_VERSION = 'v2';

    /**
     * Get Ticker for trading pair (ex: BTC/USD).
     *
     * @param string $tradingPair
     *
     * @return Ticker
     */
    public function ticker(string $tradingPair): Ticker
    {
        $result = $this->publicRequest('ticker', $this->convertTradingPairToUrlSymbol($tradingPair));

        return new Ticker($result);
    }

    /**
     * Get Hourly Ticker for trading pair (ex: BTC/USD).
     *
     * @param string $tradingPair
     *
     * @return Ticker
     */
    public function hourlyTicker(string $tradingPair): Ticker
    {
        $result = $this->publicRequest('ticker_hour', $this->convertTradingPairToUrlSymbol($tradingPair));

        return new Ticker($result);
    }

    /**
     * Get Order Book for trading pair (ex: BTC/USD).
     *
     * @param string $tradingPair
     * @param bool   $grouped Optionally, open orders are not grouped at same price - default: grouped
     * @param bool   $withMicroTimestamp Optionally, if Open orders are not grouped, get order ids as microtimestamp - default: without microtimestamp
     *
     * @return OrderBook
     */
    public function orderBook(string $tradingPair, bool $grouped = true, bool $withMicroTimestamp = false): OrderBook
    {
        /**
         * 0: orders are not grouped at same price
         * 1: orders are grouped at same price
         * 2: orders with their order ids are not grouped at same price
         */
        $parameterGroup = (true === $grouped ? 1 : (false === $withMicroTimestamp ? 0 : 2));

        $result = $this->publicRequest('order_book', $this->convertTradingPairToUrlSymbol($tradingPair), ['group' => $parameterGroup]);

        return new OrderBook($result);
    }

    /**
     * Get Trading pairs information.
     *
     * @param string $keyBy Optionnally, key the returned collection by one of the API field (name, url_symbol, ...) - default: name
     *
     * @return TradingPairCollection
     */
    public function tradingPairsInformation(string $keyBy = 'name'): TradingPairCollection
    {
        $result = $this->publicRequest('trading-pairs-info');

        if (is_array($result)) {
            return (new TradingPairCollection($result))
                ->keyBy($keyBy)
                ->mapInto(TradingPair::class);
        }

        return new TradingPairCollection();
    }

    /**
     * Get Trading pair information (ex: BTC/USD).
     *
     * @param string $tradingPair
     *
     * @return TradingPair
     */
    public function tradingPairInformation(string $tradingPair): ?TradingPair
    {
        return $this->tradingPairsInformation()->get($tradingPair);
    }

    /**
     * Make public request.
     *
     * @param string $function
     * @param array  $tradingUrlSymbol Optionnally, add trading URL symbol (needed for most API functions)
     * @param array  $parameters Optionnally, array of parameters - default: empty array
     * @param string $method Optionnally, set the HTTP method for the API function call - default: GET
     *
     * @return array
     *
     * @see \Illuminate\Http\Client\Factory
     */
    protected function publicRequest(string $function, string $tradingUrlSymbol = '', array $parameters = [], string $method = 'GET'): array
    {
        try {
            $url = $this->buildApiUrl($function, $tradingUrlSymbol);
            $response = Http::{Str::lower($method)}($url, $parameters);

            if ($response->successful()) {
                return $response->json();
            } else {
                $response->throw();
            }
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * Build API URL.
     *
     * @param string $function
     * @param string $tradingUrlSymbol Optionnally, add trading URL symbol (needed for most API functions)
     *
     * @return string
     */
    private function buildApiUrl(string $function, string $tradingUrlSymbol = ''): string
    {
        return sprintf('%s/%s/%s%s', static::API_URL, static::API_VERSION, $function, (('' !== $tradingUrlSymbol) ? sprintf('/%s', $tradingUrlSymbol) : ''));
    }

    /**
     * Convert trading pair to URL symbol (ex: BTC/USD => btcusd).
     *
     * @param string $tradingPair
     *
     * @return string
     */
    private function convertTradingPairToUrlSymbol(string $tradingPair): string
    {
        return Str::lower(str_replace('/', '', $tradingPair));
    }
}
